#!/bin/sh

# pour chaque dossier (sauf lib et images) dans public :
#   ajouter en titre 3 le nom du dossier (si le dossier contiens un fichier event.md en extraire le (premier) titre 1 et le mettre en libellé à la place du nom de dossier)
#   puis pour chaque sous dossier :
#     ajouter un élément de liste avec comme lien l'url cible gitlab-pages et en intitulé de lien le (premier) titre 1 du fichier slideshow.md contenu dans le sous dossier
#    
# remplacer dans README.md "## Liste des présentations" et ce qui suis par "## Liste des présentations" et ce qui à été généré plus haut.

urlRoot="https://librelois.duniter.io/slides"
res=""

for folder in `ls public/`
    do
      case $folder in
        "images") ;;
        "lib") ;;
        "common.css") ;;
        *)
          if test "!!nothing!!" = "`sed -n -e 's/^\#* *\(.*\)$/\1/p' public/${folder}/event.md 2>/dev/null || echo "!!nothing!!"`"
            then res="${res}\n### $folder\n\n"
          else res="${res}\n### `sed -n -e 's/^\#* *\(.*\)$/\1/p' public/${folder}/event.md`\n\n"
          fi
          for slide in `ls public/$folder/`
            do
              if test "event.md" != $slide
                then res="${res}- [`sed -n -e 's/^\##* *\(.*\)$/\1/p' public/${folder}/${slide}/slideshow.md | head -n 1`](${urlRoot}/${folder}/${slide}/)\n"
              fi
              if test "!!nothing!!" != "`cat public/${folder}/${slide}/video.md 2>/dev/null || echo "!!nothing!!"`"
                then res="${res} `cat public/${folder}/${slide}/video.md`\n"
              fi

          done
          ;;
      esac
done

#perl -i -0pe "s;<\!-- start auto listing -->(.*\s)*<!-- end auto listing -->;<!-- start auto listing -->${res}<!-- end auto listing -->;" README.md

sed -i -zE -e "s;<\!-- start auto listing -->(.*\s)*<\!-- end auto listing -->;<!-- start auto listing -->;" README.md
echo "${res}<!-- end auto listing -->" >> README.md

git add README.md