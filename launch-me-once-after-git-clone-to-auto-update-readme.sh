#!/bin/sh

# to launch me, type in terminal : sh launch-me-once-after-git-clone-to-auto-update-readme.sh

cp launch-me-once-after-git-clone-to-auto-update-readme.sh .git/hooks/pre-commit
chmod 755 .git/hooks/pre-commit

sh update-readme.sh