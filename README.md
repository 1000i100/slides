# Librelois slides

[This repository](https://git.duniter.org/librelois/slides) contains all* the slides of the presentations and conferences of `librelois` in relation to libre money.

[Ce dépôt](https://git.duniter.org/librelois/slides) regroupe toutes* les slides des présentations et conférences de librelois en rapport avec la monnaie libre.

*since 2019 / depuis 2019

**After git clone run :**
```shell script
sh launch-me-once-after-git-clone-to-auto-update-readme.sh
```

## Liste des présentations
<!-- start auto listing -->

### École 42 (13 novembre 2019)

- [La cryptomonnaie libre Ğ1: une blockchain atypique](https://librelois.duniter.io/slides/42l/2019-11-13-g1/)

### RML 13 (mai 2019)

- [DURS (DUniter-RuSt)](https://librelois.duniter.io/slides/rml13/durs/)
- [Serveur Blockchain minimal](https://librelois.duniter.io/slides/rml13/min-bc-server/)

### RML 14 (novembre 2019)

- [Dunitrust (Duniter-Rust)](https://librelois.duniter.io/slides/rml14/dunitrust/)
- [Public Key Secure Transport Layer](https://librelois.duniter.io/slides/rml14/pkstl/)

### Rustkathon 1 (mars 2019)

- [Le protocole DUP](https://librelois.duniter.io/slides/rustkathon2019_1/dup-protocol/)
  (Replay [peertube](https://tube.p2p.legal/videos/watch/d88f37e6-99b3-41cb-ba9a-2a1bd3c57be0) | [youtube](https://www.youtube.com/watch?v=1ZOb7XDk3Dc))
- [DURS (DUniter-RuSt)](https://librelois.duniter.io/slides/rustkathon2019_1/durs/)
- [Écosystème technique de la Ğ1](https://librelois.duniter.io/slides/rustkathon2019_1/g1-ecosystem/)
- [Rust & Web Assembly](https://librelois.duniter.io/slides/rustkathon2019_1/wasm/)

### Rustkathon 2 (octobre 2019)

- [Dunitrust (Duniter-Rust)](https://librelois.duniter.io/slides/rustkathon2019_2/dunitrust/)
- [Atelier TDD (Test Driven Development)](https://librelois.duniter.io/slides/rustkathon2019_2/tdd/)
<!-- end auto listing -->
