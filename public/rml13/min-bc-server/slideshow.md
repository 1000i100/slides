class: dark, middle, center

# Serveur Blockchain minimal

Protocoles Duniter/Ğ1  
Présentation fonctionnelle d'un serveur blockchain minimal

![duniter logo](../../images/Duniter.128.png)
![durs logo](../../images/Durs.128.png)

_23 mai 2019_  
_RML #13_  
_Librelois <elois@ifee.fr>_

Suivre la présentation sur votre écran :

`librelois.duniter.io/slides/rml13/min-bc-server`

---

layout: true
class: dark

.center[Serveur blockchain minimal]

---

## Sommaire

* [DUBP et DUNP](#dubp-and-dunp)
* [Etat vs transformation](#state-vs-transformation)
* [Les Documents Utilisateur](#user-document)
  * [Identité](#identity)
  * [Document d'adhésion](#membership)
  * [Certification](#certification)
  * [Revocation](#revocation)
  * [Transaction](#transaction)
* [Les sources de monnaie](#sources)
* [Découpage fonctionnel](#main-parts)
* [Réception d'un document](#document-receipt)
* [Synchronisation](#sync)
* [Génération d'un block](#block-generate)
* [Réception d'un block](#block-receipt)
* [Validation Locale vs Globale](#local-vs-global)
* [Les index](#index)
* [les événements](#events)
* [Résolution des Fork](#resolution-fork)
* [Le Protocole réseau](#dunp)

---

name: dubp-and-dunp

# .center[DUBP et DUNP]

Historiquement DUP désignait uniquement le protocole **blockchain**.

-> Or une crypto-monnaie **ne peut pas** fonctionner sans protocole réseau.

DUBP : Blockchain Protocol (v11) : [spécifications](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md)

DUNP : Network Protocol (v1) : [spécifications](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0004_ws2p_v1.md)

---

name: state-vs-transformation

# .center[Etat vs transformation]

La blockchain ne comporte que des transformations d'états.

<div class="mermaid">
graph LR
    bloc-genesis-->E1..En
    E1..En-->E'1..E'n
    etat0-->etat1
    etat1-->etat2
</div>

Block = transformation = liste d'événements.

Etat = Index -> persistés en base de donnée

Appliquer un bloc = Appliquer les transformations du bloc aux index de la monnaie.

=> Les noeuds Duniter/Durs/Juniter utilisent les index, pas la blockchain.

---

name: user-document

# .center[Les Documents Utilisateur]

Chaîne de caractères signée par la clé privée de l'utilisateur.

5 types de documents utilisateur :

* Identity
* Membership
* Certification
* Revocation
* Transaction

Exemple de chaque document sur [dup-tools-front](https://dup-tools-front.duniter.io/)

--

Différents formats :

* Format raw ou plain text : pour signer/vérifier
* Format compact text : dans un block
* Format JSON : message WS2Pv1

---

name: identity

# .center[Document Identité]

-> Publié une seule fois à l'entrée dans la toile.  
-> Valable à vie (jusqu'à révocation).

```properties
Version: 10
Type: Identity
Currency: duniter_unit_test_currency
Issuer: DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV
UniqueID: tic
Timestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==
```

-> Identifiant unique = triplet (Issuer, UniqueID, Timestamp)

Issuer  
UniqueID := Username  
Timestamp := Blockstamp

---

name: membership

# .center[Document d'adhésion]

-> Publié à l'entrée puis à chaque renouvellement d'adhésion.
-> Valable `msValidity` secondes (1 an pour la Ğ1).

```properties
Version: 10
Type: Membership
Currency: duniter_unit_test_currency
Issuer: DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV
Block: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
Membership: IN
UserID: tic
CertTS: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
s2hUbokkibTAWGEwErw6hyXSWlWFQ2UWs2PWx8d/kkElAyuuWaQq4Tsonuweh1xn4AC1TVWt4yMR3WrDdkhnAw==
```

-> Identifiant unique = quadruplet (Issuer, UserID, Block, CertTS)

UserID := Username  
Block := Blockstamp  
CertTS := Identity Blockstamp

---

name: certification

# .center[Certification]

-> Valable `sigValidity` secondes (2 ans pour la Ğ1).

```properties
Version: 10
Type: Certification
Currency: g1-test
Issuer: 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH
IdtyIssuer: mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg
IdtyUniqueID: mmpio
IdtyTimestamp: 7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A
IdtySignature: SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA==
CertTimestamp: 167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038
wqZxPEGxLrHGv8VdEIfUGvUcf+tDdNTMXjLzVRCQ4UhlhDRahOMjfcbP7byNYr5OfIl83S1MBxF7VJgu8YasCA==
```

-> Identifiant unique = quintuplet (Issuer, CertTimestamp, IdtyIssuer, IdtyUniqueID, IdtyTimestamp)

CertTimestamp := document Blockstamp  
IdtyUniqueID := target identity Username  
IdtyTimestamp := target identity Blockstamp  

---

name: revocation

# .center[Document de Revocation]

-> Publié une seule fois.  
-> Valable at vitam aeternam (irréversible).

```properties
Version: 10
Type: Revocation
Currency: g1
Issuer: DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV
IdtyUniqueID: tic
IdtyTimestamp: 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855
IdtySignature: 1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==
XXOgI++6qpY9O31ml/FcfbXCE6aixIrgkT5jL7kBle3YOMr+8wrp7Rt+z9hDVjrNfYX2gpeJsuMNfG4T/fzVDQ==
```

-> Identifiant unique = triplet (Issuer, IdtyUniqueID, IdtyTimestamp)

IdtyUniqueID := Username  
IdtyTimestamp := Blockstamp

---

name: transaction

# .center[Document Transaction]

```properties
Version: 10
Type: Transaction
Currency: g1
Blockstamp: 107702-0000017CDBE974DC9A46B89EE7DC2BEE4017C43A005359E0853026C21FB6A084
Locktime: 0
Issuers:
Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC
Inputs:
1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937
1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:105214
Unlocks:
0:SIG(0)
1:SIG(0)
Outputs:
2000:0:SIG(DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG)
4:0:SIG(Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC)
Comment: merci pour xxx
lnpuFsIymgz7qhKF/GsZ3n3W8ZauAAfWmT4W0iJQBLKJK2GFkesLWeMj/+GBfjD6kdkjreg9M6VfkwIZH+hCCQ==
```

-> Identifiant unique = hash sha256 du format raw

Issuers : signataires
Inputs : sources consommées
Unlocks : preuves de consommabilité des sources
Outputs : sources créées

---

name: transaction2

# .center[Document Transaction]

```properties
Issuers:
Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC
Inputs:
1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937
1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:105214
Unlocks:
0:SIG(0)
1:SIG(0)
Outputs:
2000:0:SIG(DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG)
4:0:SIG(Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC)
```

Issuers : signataires  
Inputs : sources consommées  
Unlocks : preuves de consommabilité des sources  
Outputs : sources créées

Source := ensemble de Ğ1 d'une même origine (atomique).

---

name: sources

# .center[Les sources de monnaie]

## .center[création]

2 types de sources :

1. Les Dividendes Universels :

    Créés dans un block contenant un champ '`dividend`' valorisé.
    Un tel block est généré toutes les '`dt`' secondes (86400 pour la Ğ1).

2. Les UTX0 (Unused Transaction Output) :

    ```yml
    AMOUNT:BASE:CONDITIONS
    30000:0:SIG(PUBKEY)
    10500:0:(SIG(PUBKEY1) && XHX(HASH)) || SIG(PUBKEY2)
    500000:0:SIG(PUBKEY1) || (SIG(PUBKEY2) && CSV(DELAY))
    800000:0:SIG(PUBKEY1) || (SIG(PUBKEY2) && CLTV(TIMESTAMP))
    ```

---

name: sources2

# .center[Les sources de monnaie]

## .center[preuve d'utilisabilité]

Preuves publiées dans la section 'Unlocks' de la transaction

* Format des preuves d'une source :

    ```yml
    INPUT_INDEX:PROOF1 PROOF2 .. PROOFn
    ```

* Format d'une preuve :

    ```yml
    SIG(ISSUER_INDEX)
    XHX(CODE)
    ```

---

name: sources3

# .center[Les sources de monnaie]

## .center[destruction]

Destruction actée par la déclaration dans un input d'une transaction :

1. Les Dividendes Universels :

    ```yml
    AMOUT:BASE:D:PUBLIC_KEY:BLOCK_NUMBER
    1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937
    ```

2. Les UTX0 (Unused Transaction Output) :

    ```yml
    AMOUNT:BASE:T:TX_HASH:OUTPUT_INDEX
    80000:0:T:A5415E194A6A39D9CEC6D06C797D6AB3169BF9C14E65133F42F0E4347847B3EE:0
    ```

---

name: main-parts

# .center[schéma général]

<div class="mermaid">
graph LR
    W(Inter-nodes API) --- B(Blockchain)
    W --- P(Mempool)
    P-->G(Block generator)
    B --- G
    C(Client API) --- P
    C --- B
    C --- W
    C --- I(Client indexer)
    B --> I
</div>

Blockchain : Vérifie la validité des blocks et les "applique".  

--
Block generator : génère contenu prochain block + PoW  

--
Client API : reçoit doc + req (histo tx, sources/compte, etc)

--
Client indexer : indexe sources et historique tx par compte.

--
inter-node API : synchro avec les autres noeuds.  
Mempool : Stocke documents utilisateurs temporairement.

---

name: document-receipt

# .center[Réception d'un document (cas 1)]

<div class="mermaid">
sequenceDiagram
    participant clients
    clients->>local node: user document
    local node->>local node: Check document validity
    local node-->>clients: Ok, I saved your document well!
    local node->>local node: Add document to local mempool
    local node->>others nodes: Hi, please relay this document
</div>

---

name: document-receipt2

# .center[Réception d'un document (cas 2)]

<div class="mermaid">
sequenceDiagram
    participant local node
    one other node->>local node: Hi, please relay this document
    local node->>local node: Check if already have
    local node->>local node: Check document validity
    local node->>local node: Add document to local mempool
    local node->>all others nodes: Hi, please relay this document
</div>

---

name: sync

# .center[Synchrnonisation]

<div class="mermaid">
sequenceDiagram
    local node->>others nodes: Give me the blockchain
    others nodes-->>local node: [blocks]
    loop blocks
        local node->>local node: Apply block
    end
</div>

---

name: block-generate

### .center[Génération d'un block]

<div class="mermaid">
sequenceDiagram
    local node->>local node: generate next block from sandbox
    loop PoW
        local node->>local node: increment nonce
    end
    alt find good hash
        local node->>others nodes: I'm find a valid block!
        local node-->>local node: send block to myself
    else too late
        others nodes-->>local node: new valid block!
    end
</div>

---

name: block-receipt

### .center[Réception d'un block]

<div class="mermaid">
sequenceDiagram
    local node->>local node: Local validation
    local node->>local node: Global validation (check all rules)
    alt valid
        local node->>local node: Apply block
        local node->>others nodes: relay valid block
    else invalid
        local node->>local node: tag blockstamp as invalid
    end
</div>

---

name: local-vs-global

## .center[Validation Locale vs Globale]

### -> Validation locale

* Vérifie cohérence d'un bloc bien formaté, sans autre contexte que le bloc lui-même.

### -> Validation Globale

* Vérifie cohérence d'un bloc validé localement, dans le contexte de l'ensemble de la blockchain.
* S'effectue via l'application de **108 règles** spécifiées [*ici*](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#br_g01-headnumber).
* C'est dans cette phase que les "index" entrent en jeux.

---

name: index

# .center[les index]

* IINDEX : index des identités
* MINDEX : index des adhésions
* CINDEX : index des certifications
* SINDEX : index des sources de monnaie

---

name: iindex

# .center[Index des identités]

```dump
│ op     │ uid                        │ pub                                          │ hash                                                             │ sig                                                                                      │ created_on                                                              │ written_on                                                              │ member │ wasMember │ kick │
├────────┼────────────────────────────┼──────────────────────────────────────────────┼──────────────────────────────────────────────────────────────────┼──────────────────────────────────────────────────────────────────────────────────────────┼─────────────────────────────────────────────────────────────────────────┼─────────────────────────────────────────────────────────────────────────┼────────┼───────────┼──────┼
│ CREATE │ Alfybe                     │ 3QLkBNoCNJENY8HyCDh1kDG2UKdg3q66z1Q91hpSJinD │ 1505A45A5EEBFC3AFAD1475A4739C8447A79420A83340559CE5A49F9891167BB │ 2vfmih7xhW/QLJ85PZH1Tc6j5fooIXca+yr/esnt0yvdk5LhEKrOB32JFqCctAoRNwpRjBdZ2Q8l15+In1rrDg== │ 0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855      │ 0-000003D02B95D3296A4F06DBAC51775C4336A4DC09D0E958DC40033BE7E20F3D      │ 1      │ 1         │ 0    │
```

op : CREATE or UPDATE  
uid : Username nullable  
pub : Public key  
hash : Identity hash sha256(uid ++ pub ++ created_on) nullable  
sig : identity signature nullable  
created_on : Blockstamp nullable  
written_on : Blockstamp  
member : Boolean nullable  
wasMember : Boolean nullable  
kick : Boolean nullable

---

name: mindex

# .center[Index des adhésions]

```dump
│ op     │ pub                                          │ created_on                                                              │ written_on                                                              │ expires_on │ expired_on │ revokes_on │ revoked_on                                                              │ leaving │ revocation                                                                               │ chainable_on │
├────────┼──────────────────────────────────────────────┼─────────────────────────────────────────────────────────────────────────┼─────────────────────────────────────────────────────────────────────────┼────────────┼────────────┼────────────┼─────────────────────────────────────────────────────────────────────────┼─────────┼──────────────────────────────────────────────────────────────────────────────────────────┼──────────────┤
│ CREATE │ ENmp3e9Q2uXHx4Y666Th615j3fszLfXZsZ5KwZ9sV6R6 │ 31025-000005D59EB5D78E60AB08A82DF6B5BF2DEAF4939CB7285DD5F82AE8751EE4F8  │ 32117-00000329FDB226430FE949D4B8CF39CA783D22D0ACFFD6CCFC14F02A49BE4716  │ 1530242547 │ NULL       │ 1561800147 │ NULL                                                                    │ 0       │ NULL                                                                                     │ 1504288331   │
```

op : CREATE or UPDATE  
pub : Public key  
created_on : Blockstamp nullable  
written_on : Blockstamp  
expires_on : Timetamp nullable  
expired_on : Timetamp nullable  
revokes_on : Timetamp nullable  
revoked_on : Blockstamp nullable  
leaving : Boolean nullable  
revocation : Signature nullable  
chainable_on : Timetamp nullable

???

expired_on : vaut parfois zéro au lieu de null: cas d'une identité expirée qui redevient membre

---

name: cindex

# .center[Index des certifications]

```dump
│ op     │ issuer                                       │ receiver                                     │ created_on │ written_on                                                              │ sig                                                                                      │ expires_on │ expired_on │ chainable_on │ replayable_on │
├────────┼──────────────────────────────────────────────┼──────────────────────────────────────────────┼────────────┼─────────────────────────────────────────────────────────────────────────┼──────────────────────────────────────────────────────────────────────────────────────────┼────────────┼────────────┼──────────────┼───────────────┤
│ CREATE │ 2ny7YAdmzReQxAayyJZsyVYwYhVyax2thKcGknmQy5nQ │ 2rn7CzTA7d2anK2W3cpEyNpcs8r8DEZ95PMdcMz8aETi │ 0          │ 0-000003D02B95D3296A4F06DBAC51775C4336A4DC09D0E958DC40033BE7E20F3D      │ hHF4DSw6NS98CY5R5LZZBnD4WKnopX2gmohKyLQi2L+FZw7Gh+ykVXF0f+TO58urZ+IPtoMASSWAf0NzWe8qAw== │ 1552102327 │ 0          │ 1489419127   │ 1494246727    │
```

op : CREATE  
issuer : Public key  
receiver : Public key  
created_on : Block number  
written_on : Blockstamp  
sig : Signature  
expires_on : Timestamp  
expired_on : vaut toujours zéro  
chainable_on : Timestamp  
replayable_on : Timestamp

---

name: sindex

# .center[Index des sources]

```dump
│ op     │ tx                                                               │ identifier                                                       │ pos    │ created_on                                                              │ amount │ base │ locktime │ consumed │ conditions                                        │ writtenOn │
├────────┼──────────────────────────────────────────────────────────────────┼──────────────────────────────────────────────────────────────────┼────────┼─────────────────────────────────────────────────────────────────────────┼────────┼──────┼──────────┼──────────┼───────────────────────────────────────────────────┼───────────┤
│ CREATE │ NULL                                                             │ 7wpm4s4o6SyWpvdDfUekyVw69rbuYfKMHA4VBL3TD3Zs                     │ 1      │ NULL                                                                    │ 1000   │ 0    │ NULL     │ 0        │ SIG(7wpm4s4o6SyWpvdDfUekyVw69rbuYfKMHA4VBL3TD3Zs) │ 1         │
```

op : CREATE or UPDATE  
tx : si CREATE : Hash tx de création (ou Null si source DU)  
tx : si UPDATE : Hash tx de consommation  
identifier : TxHash or IssuerPubkey  
pos : OutputIndex or BlockNumber  
created_on : Blockstamp nullable (not-null UPDATE only)  
amount : Integer (centimes d'unité monétaire)  
base : integer (Puissance de 10)  
locktime : Integer (Nombre de secondes)  
consumed : Boolean  
conditions : String (Conditions de consommabilité)  
writtenOn : BlockNumber

---

name: events

# .center[les événements]

2 types d'événements :

* Evenements utilisateurs : provoqués par un Document signé.
* Évenements spontannés : provoqués par temps blockchain.

Les événements utilisateurs :

* **identities** : écriture d'une identité -> IINDEX + MINDEX
* **joiners** : entrée d'un membre -> IINDEX + MINDEX
* **actives** : renouvellement d'un membre -> MINDEX
* **leavers** : départ d'un membre -> MINDEX
* **revoked** : révocation d'un membre -> MINDEX
* **certifications** : écriture d'une certification -> CINDEX
* **transactions** : écriture d'une transaction -> SINDEX

Les événements spontanés :

* **excluded** : exclusion d'un membre -> IINDEX
* **certExpire** : expiration d'une certification -> CINDEX
* **implicitlyRevoked** : révocation d'un membre -> MINDEX

???

3ème cause possible d'un event : provoqué par un autre.
Par exemple l'event 'excluded' est provoqué par les event 'revoked' et 'implicitlyRevoked'.

---

name: resolution-fork

## .center[Résolution des Fork]

R1 : La branche la plus longue l'emporte.  
R2 : Pour changer de branche, la nouvelle doit avoir au moins 3 blocs d'avance **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs inclus.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3-->A2
        B2-->A1
        C3-->A2
        C4-->C3
        C5-->C4
</div>

--
### .center[=> Le node A reste sur sa branche]

---

## .center[Résolution des Fork (2)]

R1 : La branche la plus longue l'emporte.  
R2 : Pour changer de branche, la nouvelle doit avoir au moins 3 blocs d'avance **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs inclus.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3-->A2
        B2-->A1
        B3-->B2
        B4-->B3
        B5-->B4
        B6-->B5
        C3-->A2
        C4-->C3
        C5-->C4
</div>

--
#### .center[=> Le node A rollback 2 blocs puis empile la chaîne B]

---

## .center[Résolution des Fork (3)]

R1 : La branche la plus longue l'emporte.  
R2 : Pour changer de branche, la nouvelle doit avoir au moins 3 blocs d'avance **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs inclus.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3..A101-->A2
        A102-->A3..A101
        B2-->A1
        B3..B103-->B2
        B104-->B3..B103
        B105-->B104
        B106-->B105
        C3..C103-->A2
        C104-->C3..C103
        C105-->C104
</div>

--
#### .center[=> Le node A rollback 100 block puis empile la chaîne C]

---

name: dunp

# .center[Le protocole réseau]

* Basé sur WS2P (WebSocket To Peer) v1 : [spécifications](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0004_ws2p_v1.md)
* connexions permanentes pour synchroniser :
  * les blocks
  * les documents utilisateurs
  * les mempool des nodes
  * les fiches de peer
* 2 types de nodes : WS2P Public & WS2P Privé
  * WS2P public = avec endpoint
  * WS2P Privé = sans endpoint
* Les nodes avec WS2P Public ont aussi WS2P Privé d'activé.
* Chaque node a un quota de connexions limités (faible en entrée et élevé en sortie).

-> [Présentation de WS2Pv2](https://librelois.duniter.io/ws2pv2-rml12/)

---

# .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de Duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]
