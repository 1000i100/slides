class: dark, middle, center

## Dunitrust (Duniter-Rust)

![durs logo](../../images/Durs.png)

_5 octobre 2019_  
_Rustkathon #2_  
_Librelois <c@elo.tf>_

Suivre la présentation sur votre écran :

`librelois.duniter.io/slides/rustkathon2019_2/dunitrust`

---

layout: true
class: dark

.center[Dunitrust (Duniter-Rust)]

---

## Sommaire

1. [Pourquoi Dunitrust](#why)
2. [Historique du projet](#history)
3. [Codebase](#codebase)
4. [État fonctionnel](#functional-report)
5. [Depuis les rml13](#since-rml13)
6. [Roadmap pour les rml14](#roadmap-rml14)
7. [A faire en priorité](#todo-priority)
8. [Architecture du projet](#archi)
9. [Le système de modules](#module-system)
10. [Structure des données](#bc-db-1)
11. [Lecture BDD](#bc-db-r)
12. [Écriture BDD](#bc-db-w)
13. [Lecture BDD: une requête](#bc-db-rq)
14. [Écriture BDD: une requête](#bc-db-wq)

---

name: why

## .center[Pourquoi Dunitrust]

![durs logo](../../images/Durs.64.png)

--

- Pour le plaisir de le faire
--

- Sécurité par la diversité (pas les mêmes failles)
- Haute dispos (relaye si bug critique sur l'autre implémentation)
--

- Oblige des spécifications DUP plus détaillés (donc moins de bug)
- 2 points de vues réduit drastiquement les oublis
- 2 fois plus de contributeurs touchables
--

- Gains de Rust :
  - Passage a l'échelle (Rust ultra performant & scalable)
  - Fiabilité (memory leaks, data races)
  - Ultra-light (bien pour les nano-pc, raspi zéro, etc)
--

- Réduire le bus factor :
  - Projet pensé collectif dés le départ
  - Priorise l'évolution horizontale plutôt que verticale

???

Projet très long-terme
On prend le temps de faire les choses bien et de former le maximum de monde.

---

name: history

## .center[Historique du projet]

- Novembre 2017 Création du projet par Nanocryk.
  - ~ 3000 lignes. Crates crypto et wot.
- Février 2018: Rejoins par Éloïs.
- Mai 2018: Nanocryk quitte le projet pour prioriser `Fygg`.
- Fin mai 2018: [1ère prés. officielle de duniter-rs aux rml11](https://www.youtube.com/watch?v=fJvMRv1l5wM)
- Juillet 2018: v0.1
- Septembre 2018 : Renommage Duniter-Rs -> Durs (demande cgeek)
- Octobre 2018 : Nouveau contributeur: inso.
- Novembre 2018: [2ème prés. aux rml12](https://www.youtube.com/watch?v=EIjJNTeaP2g)
- Décembre 2018: Nouveau contributeur ji_emme.
- Mars 2019 : 1er hackathon dédié au projet.
- Avril 2019 : 3 nouveaux contributeurs : Hugo, Jonas et Tuxmain.
- Mai 2019: v0.2 + [3ème prés. aux rml13](https://www.youtube.com/watch?v=)
- Juin 2019: Nouveau contributeur: Lucas.
- Octobre 2019: 2ème hackathon dédié au projet.

---

name: codebase

## .center[Codebase]

- 31_167 lignes de code
  - Éloïs 36_352 (84%)
  - Nanocryk 5629 (13%)
  - inso 622 (1.5%)
  - Hugo 267 (0.6%)
  - Jonas 136 (0.3%)
  - ji_emme 46 (0.1%)
  - Moul 27 (0.05%)
- 30 crates (29 lib + 1 bin)

- ~800h de travail sur 18 mois

---

name: functional-report

## .center[État fonctionnel]

- Synchronisation rapide depuis une blockchain JSON en local
- Un nœud miroir qui reste synchronisé sur le réseau
- une base de donnée indexant la blockchain
- WS2P Privé
- une interface graphique dans le terminal
- Un explorateur de BDD fournissant :
  - date d'expiration des membres
  - règle de distance des membres
  - solde d'un compte
  - liste des certifications d'un membre

---

name: since-rml13

## .center[Depuis les rml13]

--

- Refactor du core pour préparer la sync par le réseau
--

- Externalisation des paramètres monétaires dans la crate currency-params
--

- Versionnage des blocs et des documents wot.
--

- Crypto: Complétion des tests pour couvrir toute la crate.
--

- Blockchain: requetage des blocs précédents pour remplir les trous dans l'arbre des forks.
--

- Restructuration de l'organisation des crates (et découpage crate documents)
--

- Ajout de la crate dubp-indexes (décrit les indexs dans le format du protocole)
--

- Dbex: ajout de la sous-commande blocks (ji_emme)
--

- Crypto: refonte pour utiliser ring (+ clear secrets at drop)
--

- Développement de la couche de sécurité PKSTL.
--

- Ajout du build d'un paquet arch (tuxmain)
--

- Ajout d'un audit automatique dans la CI (détection des failles de sécurités).
--

- Système de persistance des données: migration rustbreak -> lmdb
--

=> ~ 250h de travail sur 4 mois.

---

name: roadmap-rml14

## .center[Roadmap pour les rml14]

- Jalon 0.3 :
  - Tests d'intégration de la synchro rapide, export des index (elois).
  - WS2P v2 synchronisation de la blockchain par le réseau (Hugo).

- Jalon 0.4 :
  - Commencer le client indexer (elois).
  - Poser les bases de la nouvelle API Client GraphQL (ji_emme et jsprenger).
  - WS2P v2 rebond des fiches de peer, HEADs, et documents. (Hugo).
  - Module GVA : nouvelle API Client GraphQL (ji_emme et jsprenger).

---

name: todo-priority

## .center[Taches libres]

- Daemonisation.

- Vérification globale : appliquer toutes les règles du protocole DUBP.

- Module MemPool : sandbox des documents.

- dbex: extraite des statistiques de la DB.

- Externaliser et binder PKSTL.

- Documenter au maximum l'existant.

- Traduire les documentations.

--

=> Formation et transmission de connaissances via talk mumble.

---

name: archi

## .center[architecture  du dépôt]

- .github : messages pour le dépot mirroir
- .gitlab : scripts python de publication des releases
- bin/ : crates binaires (durs-server, durs-desktop)
- doc/ : documentation haut niveau (pour dev ET utilisateurs)
- images/ : images statiques
- lib/ : crates bibliothèques (tools, core, modules)
  - core/ : crates constituant le squelette du programme (conf, boot, système modulaire)
  - dubp/ : Bibliothèques déclarant les entités du protocole DUBP.
  - dunp/ : Bibliothèques déclarant les entités du protocole DUNP.
  - modules/ : crates des modules
  - modules-lib/ : Bibliothèques utilisées par plusieurs modules.
  - tools : Bibliothèques externalisables (code non lié a l'architecture de durs)
- releases/ : scripts bach de build des paquets
- target/ : contient les binaires générés (ignoré par git)

---

name: module-system

## .center[Le système de modules (1/5)]

![durs-modules-step-0](../../images/durs-modules-step-0.png)

- Chaque module est cloisonné dans un thread dédié.
- Ils interagissent uniquement par channels rust.

---

## .center[Le système de modules (2/5)]

![durs-modules-step-0](../../images/durs-modules-step-1.png)

- Le cœur lance les modules : ModName::start(..., ...)
- start() prend en paramètre le RooterSender (+conf, etc)

---

## .center[Le système de modules (3/5)]

![durs-modules-step-0](../../images/durs-modules-step-2.png)

-> Chaque module s'enregistre auprès du router en transmettant une struct `ModuleRegistration`.

---

## .center[Le système de modules (4/5)]

![durs-modules-step-0](../../images/durs-modules-step-3.png)

- Puis les modules transmettent leurs messages au router.
- 3 types de messages : Event, Request, Response.

---

## .center[Le système de modules (5/5)]

![durs-modules-step-0](../../images/durs-modules-step-4.png)

- Le router filtre et relaie au(x) module(s) concernés.
- Destinataires ciblés via types de rôles et events.

---

name: bc-db-1

## .center[Structure des données]

Paradigme Clé-Valeur. Chaque collection est nommée store.

LMDB défini 4 types de collections/stores :

- mono-valuée (Single)
- mono-valuée avec clés entières (SingleIntKey)
- multi-valuée (Multi)
- multi-valuée avec clés entières (MultiIntKey)

On peut découper les collections/stores de Dunitrust en 4 groupes fonctionnels :

- current meta datas
- blocks (blockchain and forks)
- indexes (for global validation)
- client indexes (for client APIs)

---

name: bc-db-2

## .center[Structure des données (2)]

Current meta datas and blocks collections :

```text
Name               | StoreType    | KeyType            | ValueType
-------------------|--------------|--------------------|-----------------
CURRENT_META_DATAS | SingleIntKey | u32                | ?
MAIN_BLOCKS        | SingleIntKey | BlockNumber        | BlockDb
ORPHAN_BLOCKSTAMP  | Single       | PreviousBlockstamp | Vec<Blockstamp>
FORK_BLOCKS        | Single       | Blockstamp         | BlockDb
```

```text
       CURRENT_META_DATAS
----------------------|------------
Key                   | ValueType
DbVersion             | u64
CurrencyName          | String
CurrentBlockstamp     | Blockstamp
CurrentBlockchainTime | u64
ForkTree              | ForkTree
NextWotId             | WotId

```

---

name: bc-db-3

## .center[Structure des données (3)]

Indexes collections :

```text
Name                   | StoreType    | KeyType            | ValueType
-----------------------|--------------|--------------------|----------
WOT_ID_INDEX           | Single       | PubKey             | WotId
IDENTITIES             | SingleIntKey | WotId              | DbIdentit
MBS_BY_CREATED_BLOCK   | MultiIntKey  | BlockNumber        | WotId
CERTS_BY_CREATED_BLOCK | MultiIntKey  | BlockNumber        | u64
DIVIDENDS              | Multi        | PubKey             | BlockNumber
UTXOS                  | Single       | UniqueIdUTXOv10    | TxOutput
CONSUMED_UTXOS         | SingleIntKey | BlockNumber        | Vec<UTXO>
```

---

name: bc-db-r

## .center[Lecture BDD]

BDD 100% ACID (donc transactionnelle).

Plusieurs requêtes dans une même transaction de lecture :

```rust
use durs_bc_db_reader::blocks::get_block_in_local_blockchain;
use durs_bc_db_reader::current_meta_datas::get_current_blockstamp;

let current_block: Option<BlockDocument> = db.read(|r| {
    if let Some(current_blockstamp) =
        get_current_blockstamp(db, r)?
    {
        get_block_in_local_blockchain(
            db,
            r,
            current_blockstamp.id
        )?
    } else {
        None
    }
})?;
```

---

name: bc-db-w

## .center[Écriture BDD]

Plusieurs requêtes dans une même transaction d'écriture :

```rust
use durs_bc_db_writer::blocks::insert_new_head_block;
use durs_bc_db_writer::cur..meta_datas::update_current_meta_datas;

let new_valid_block: BlockDocument;
db.write(|mut w| {
    update_current_meta_datas(db, &mut w, &new_valid_block)?;
    insert_new_head_block(db, &mut w, None, BlockDb {
        block: new_valid_block,
        expire_certs: None,
    })?;
    Ok(w) // <- commit
})?;
```

---

name: bc-db-rq

## .center[Lecture BDD: une requête]

Récupération d'un bloc par son numéro :

```rust
/// Get BlockDb in local blockchain
pub fn get_db_block_in_local_blockchain(
    db: &DB,
    r: &R,
    block_number: BlockNumber,
) -> Result<Option<BlockDb>, DbError> where
    DB: DbReadable,
    R: DbReader,
{
    if let Some(v) = db.get_int_store(MAIN_BLOCKS)
        .get(r, block_number.0)?
    {
        Ok(Some(from_db_value(v)?))
    } else {
        Ok(None)
    }
}
```

---

name: bc-db-wq

## .center[Écriture BDD: une requête]

Insertion d'un bloc dans la blockchain locale :

```rust
/// Insert new head Block in databases
/// Update MAIN_BLOCK only
pub fn insert_new_head_block(
    db: &Db,
    w: &mut DbWriter,
    dal_block: BlockDb,
) -> Result<(), DbError> {
    // Serialize datas
    let bin_dal_block = durs_dbs_tools::to_bytes(&dal_block)?;

    // Insert block in MAIN_BLOCKS store
    db.get_int_store(MAIN_BLOCKS).put(
        w.as_mut(),
        *dal_block.block.number(),
        &Db::db_value(&bin_dal_block)?,
    )?;
    Ok(())
}
```

---

## .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]
