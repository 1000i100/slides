class: dark, middle, center

# Rust & Web Assembly

![rust logo](../../images/rust-logo-128x128.png) ![wasm logo](../../images/wasm-logo.png)

_23 mars 2019_  
_Rustkathon 1_  
_Librelois <elois@ifee.fr>_

Suivre la présentation sur votre ecran :

`librelois.duniter.io/slides/rustkathon2019_1/wasm`

---

layout: true
class: dark

.center[Rust & Web Assembly]

---

## Sommaire

1. [Qu’est-ce que WebAssembly ?](#what)
2. [Pourquoi WebAssembly ?](#why)
3. [Wasm + Rust = &#128150;](#wasm+rust)
4. [2 target  différentes](#2target)
5. [Wasm-pack](#wasm-pack)
6. [Les références](#references)
7. [Installation des outils](#install)
8. [Atelier : hello world](#hw)
9. [Atelier : binding Rust/Js](#binding)
10. [Étude d'un exemple concret : `dup-tools-wasm`](#dup-tools)

---

name: what

## .center[Qu’est-ce que WebAssembly ?]

.center[![wasm logo](../../images/wasm-logo.png)]

--

WebAssembly est un standard de binaire (byte code) compréhensible par les navigateurs **modernes** (Firefox, Chrome, Safari et même Edge).

--
> .question[C’est donc un nouveau langage ?]

--

Non, c’est un format binaire comme l’assembleur. Il a été conçu pour être utilisé avec des langages haut niveau comme le C/C++/Rust.

--

> .question[Faut-il générer un binaire pour chaque plateforme ?]

--

Non, le binaire est indépendant de la plateforme : il n’y aura pas de compilation spécifique pour ARM / x32 / x64. Une fois le byte code livré, il fonctionnera tel quel sur tous les types d’architecture !

---

name: why

## .center[Pourquoi WebAssembly ?]

> .question[WebAssembly va-t-il remplacer Javascript ?]

WebAssembly n’a pas pour but de remplacer Javascript. Il est conçu pour compléter Javascript avec des performances supérieures.  
C’est une alternative pour construire des jeux 2D / 3D, ou des applications Web devant effectuer des calculs complexes.

> .question[Quels sont les avantages de WebAssembly par rapport à Javascript ?]

- Binaire compilé -> parsing grandement facilité -> ~ 20 fois plus rapide à charger qu’un fichier javascript !
- Ce n’est pas un fichier texte donc il sera « moins » gros. Il est donc plus rapide à charger et plus rapide à exécuter.
- Plus rapide pour faire les calculs : adapté pour compression, manipulation d’images, opérations cryptographiques, jeux vidéos, etc

---

name: wasm+rust

## .center[Wasm + Rust = &#128150;]

- Compilateur Rust->Webassembly depuis le 21/12/17
- Intégration de webpack et npm depuis qq mois !!
--

- Très facile a développer avec wasm-pack !
--
name: wasm-rust
- On peut faire des jeux temps réel dans le navigateur

> [Exemple de jeu en rust-wasm](https://aochagavia.github.io/blog/rocket---a-rust-game-running-on-wasm/)

---

name: 2target

## .center[2 target  différentes]

### wasm32-unknown-emscripten

- Emscripten est utilisé pour embarquer l'émulation des I/O (TCP sockets/file io/multithreading/opengl).
- Attention : Livrable final volumineux !

### wasm32-unknown-unknown

- Target ultralight. Livrable final de quelques dizaines de Ko seulement.
- A préférer si vous utilisez webassembly comme une bibliothèque de fonctions et que vous gérez les I/O en Javascript.
- Avec crates web-sys & js-sys, 

---

name: wasm-pack

## Wasm-pack

![wasm-pack](../../images/wasm-pack-logo.png)

- Génération automatique du binding Rust/Js
- Enrobage dans un module npm !!
- Binding Typescript
- Tests automatisés avec moteurs Firefox et Chrome
- Passage en toolchain stable avec Rust 2018 edition

---

name: references

## Les références

- [wasm-bindgen book](https://rustwasm.github.io/docs/wasm-bindgen/)
- [wasm-pack book](https://rustwasm.github.io/docs/wasm-pack/)

---

name: install

## Installation des outils

1. Installer [Rust](https://www.rust-lang.org/tools/install)
2. Installer nvm ([linux/mac][nvm-linux-install] ou [windows][nvm-windows-install])
3. Installez npm via nvm (`nvm install 10.15.3`)
4. [Installer wasm-pack](https://rustwasm.github.io/wasm-pack/installer/)

[nvm-linux-install]:https://github.com/creationix/nvm#installation-and-update
[nvm-windows-install]:https://github.com/coreybutler/nvm-windows#installation--upgrades

---

name: hw

## Atelier : Hello World (1/3)

Créez un nouveau projet:

```bash
cargo new --lib my-first-wasm-lib
```

Modifiez le Cargo.toml :

```toml
[package]
name = "my-first-wasm-lib"
version = "0.1.0"
authors = ["you <you@mail.tld>"]
edition = "2018"

[lib]
crate-type = ["cdylib"] # Our library is a C compatible dynamic library

[dependencies]
wasm-bindgen = "0.2" # library for Js binding
```

---

## Atelier : Hello World (2/3)

Ajoutez le code suivant dans `src/lib.rs`

```rust
extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern {
    fn alert(s: &str); // On "capte" la fonction Js 'alert()'
}

#[wasm_bindgen]
pub fn greet() { // Fonction qui sera appellée par notre front
    alert("Hello, World!");
}
```

Buildez votre lib :

```bash
wasm-pack build
```

---

## Atelier : Hello World (3/3)

1. Créez un dépot pour votre front a partir du template wasm-app :

    ```bash
    mkdir my-first-wasm-app && cd my-first-wasm-app
    npm init wasm-app
    ```

2. Dans le `package.json`, remplacez `hello-wasm-pack` par votre lib :

    ```json
        "devDependencies": {
            "my-first-wasm-lib": "local/path/to/your/lib",
            ... // keep others dependencies
        }
    ```

3. Dans `index.js` remplacez `hello-wasm-pack` par votre lib :

    ```js
    import * as wasm from "my-first-wasm-lib";

    wasm.greet();
    ```

4. Builder et lancer votre front : `npm i && npm start`

---

name: binding

## Atelier : binding Rust/Js (1/2)

1. Créer un nouveau projet a partir du template `wasm-pack` :

    ```bash
    cargo install cargo-generate
    cargo generate --git https://github.com/rustwasm/wasm-pack-template
    ```

2. Compilez puis importez dans votre front.
3. Binder une enum, dans `src/lib.rs` :

    ```rust
    #[wasm_bindgen]
    #[derive(Debug, Copy, Clone)]
    pub enum MyEnum {
        Variant1,
        Variant2,
        ...
    }
    ```

???

Le nom du projet est demandé de manière interactive par cargo-generate.
C'est cargo-generate qui vas créer le dossier du projet.

---

## Atelier : binding Rust/Js (2/2)

1. Déclarer une fonction recevant votre enum :

    ```rust
    #[wasm_bindgen]
    pub fn my_function(variant: ) -> String {
        match variant {
            MyEnum::Variant1 => "variante 1 !".into(),
            MyEnum::Variant2 => "variante 2 !".into(),
            ...
        }
    }
    ```

2. Compilez puis utilisez cette fonction dans votre front.

3. Faite une fonction rust qui renvoie une enum. Exemple: une fonction qui reçoit un age en entrée et qui indique si la personne est enfant, active ou retraitée.

4. Libérez votre créativité !

---

name: dup-tools

### Un cas concret : `dup-tools-wasm` (1/4)

<div class="mermaid">
graph TD
        dup-crypto-->dubp-documents
        dup-crypto-->durs-network-documents
        dubp-documents-->dup-tools-wasm
        durs-network-documents-->dup-tools-wasm
</div>

- Expose des fonctions Js utiles pour manipuler les entités du protocole DUP (trousseaux de clés, signatures, documents, etc)
- Utilise 3 crates de [DURS](https://git.duniter.org/nodes/rust/duniter-rs) comme dépendance.

---

### Un cas concret : `dup-tools-wasm` (2/4)

Expose des fonctions de crypto dans [crypto.rs](https://git.duniter.org/tools/dup-tools-wasm/blob/master/src/crypto.rs).  
Exemple: fonction de vérification d'une signature:

```rust
#[wasm_bindgen]
pub fn verify(datas: &str, pubkey: &str, signature: &str) -> bool {
    let pubkey = match ed25519::PublicKey::from_base58(pubkey) {
        Ok(pubkey) => pubkey,
        Err(_) => return false,
    };
    let sig = match ed25519::Signature::from_base64(signature) {
        Ok(signature) => signature,
        Err(_) => return false,
    };

    pubkey.verify(datas.as_bytes(), &sig)
}
```

---

### Un cas concret : `dup-tools-wasm` (3/4)

Expose des fonctions de parsing des documents dans [parsers.rs](https://git.duniter.org/tools/dup-tools-wasm/blob/master/src/parsers.rs)

```rust
#[wasm_bindgen]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum DocCheckResult {
    UnknownType,
    WrongFormat,
    InvalidSig,
    ValidSig,
}

#[wasm_bindgen]
pub fn parse_doc_and_verify(source: &str, doc_type: DocumentType) -> DocCheckResult;

#[wasm_bindgen]
pub fn parse_doc_into_json(doc: &str, doc_type: DocumentType) -> String;
```

---

### Un cas concret : `dup-tools-wasm` (4/4)

Clonez le dépot :

```bash
git clone https://git.duniter.org/tools/dup-tools-wasm.git
```

Rendez vous dans le projet puis compilez et testez :

```bash
cd dup-tools-wasm
wasm-pack build
wasm-pack test --headless --firefox
```

Amusez vous a rajouter votre propre fonction.

utilisez le tout dans une wasm-app :

```bash
mkdir my-dup-tools-app && cd my-dup-tools-app
npm init wasm-app
```

---

# .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]
