class: dark, middle, center

# Écosystème technique de la Ğ1

![g1 logo](../../images/logo-g1.flare.256.png)

_23 mars 2019_  
_Rustkathon #1_  
_Librelois <elois@ifee.fr>_

Suivre la présentation sur votre ecran :

`librelois.duniter.io/slides/rustkathon2019_1/g1-ecosystem`

---

layout: true
class: dark

.center[Écosystème technique de la Ğ1]

---

## Sommaire

1. [La Ğ1 en 1 diapo.](#g1)
2. [Le protocole de la Ğ1](#dup)
3. [Écosysteme général](#global)
4. [Les serveurs blockchain](#nodes)
5. [Les clients (=wallets)](#clients)
6. [Les nombreux autres logiciels](#tools)

---

name: g1

## .center[La Ğ1 (prononcer "June")]

.center[![g1 logo](../../images/logo-g1.flare.128.png)]

1ère monnaie libre (au monde !) au sens de la [Théorie Relative de la Monnaie (TRM)](http://trm.creationmonetaire.info/).

La TRM a été publiée en 2010. Il y a eu 2 projets d'implémentation :

* 1er projet UDP en 2011 (abandonné en 2013).
* 2ème projet Ucoin né en 2013 en reprennant les bases d'UDP.
* En 2016, le projet Ucoin est renommé Duniter.
* Duniter fait fonctionner la **Ğ1** depuis le **8 mars 2017**.

La Ğ1 est co-créée par tout ces membres a part égale chaque jour. Cette part journalière de création est nommée **Dividende universel**.

---

name: dup

# .center[Le protocole de la Ğ1]

## .center[DUP (DUniter Protocol)]

Protocole **blockchain** from scratch (génèse a commencée avec UDP).

Transactions (like other cryptos) + Spécificités :

* Co-création du dividende universel
* Toile de confiance permettant de garantir l'unicité des comptes membres de manière décentralisée.

.center[![blockchain france](../../images/sybil1-r1.jpg)]

---

name: global

# .center[Écosysteme général]

.center[![g1-ecosystem](../../images/g1-ecosystem.png)]

---

name: nodes

# .center[Les serveurs blockchain]

| Logo | Nom | Langage | Avancement | Référent | Contributeurs actifs* |
|:-:|:-:|:-:|:-:|:-:|:-:|
| ![g1-ecosystem](../../images/Duniter.64.png) | Duniter | NodeJs | En prod (v1.7)  | [cgeek](https://forum.duniter.org/u/cgeek)     | 5 |
| ![g1-ecosystem](../../images/Durs.64.png) | Durs    | Rust   | En dev (v0.1.1) | [elois](https://forum.duniter.org/u/elois)     | 4 |
| - | Juniter | Java   | En dev (v0.1.0) | [Junidev](https://forum.duniter.org/u/Junidev) | 1 |

\*_Ayant réalisé au moins un commit mergé dans les 12 derniers mois._

---

name: clients

# .center[Les clients]

| Logo | Nom | Langage | Avancement | Référent | Contributeurs actifs* |
|:-:|:-:|:-:|:-:|:-:|:-:|
| ![g1-ecosystem](../../images/Cesium.64.png)       | Cesium | Js     | En prod (v1.2.9) | [kimamila](https://forum.duniter.org/u/kimamila) | 5 |
| ![g1-ecosystem](../../images/Sakia.64.png)        | Sakia  | Python | En prod (v0.33)  | [inso](https://forum.duniter.org/u/inso)              | 3 |
| ![g1-ecosystem](../../images/Silkaj.white.64.png) | Silkaj | Python | En prod (v0.6.5) | [moul](https://forum.duniter.org/u/moul)              | 5 |

\*_Ayant réalisé au moins un commit mergé dans les 12 derniers mois._

---

name: tools

## Les nombreux autres logiciels

Visualisation de données :

* g1-monit (NodeJs) : stats monnaie et wot ([elois](https://forum.duniter.org/u/elois) & [cgeek](https://forum.duniter.org/u/cgeek))
* wotwizard (Pascal) : stats wot + proba d'entrées ([gerard94](https://forum.duniter.org/u/gerard94))
* wotmap (Js) : Visualisateur de la towotile ([pierre_jean_chancell](https://forum.duniter.org/u/pierre_jean_chancell))
* WorldWotMap (python) : Projection wot sur carte ([tuxmain](https://forum.duniter.org/u/tuxmain))

Tools :

* Gsper (Js): récupérateur de pass ([1000i100](https://forum.duniter.org/u/1000i100))
* Tipǎy () : ([1000i100](https://forum.duniter.org/u/1000i100))
* G1Billet () : ([1000i100](https://forum.duniter.org/u/1000i100))
* G1sms (python) : paiement par sms : ([frederic_renault](https://forum.duniter.org/u/frederic_renault))
* dup-tools (Rust&Js): validateur de documents blockchain ([elois](https://forum.duniter.org/u/elois))
* Gmix (python) : mixeur de sources ([tuxmain](https://forum.duniter.org/u/tuxmain))

Et bien d'autres encore...

---

class: center

### Retrouvez tout les projets sur notre forge

# [git.duniter.org](https://git.duniter.org)

--

# .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]