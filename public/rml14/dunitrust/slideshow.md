class: dark, middle, center

## Dunitrust (Duniter-Rust)

![durs logo](../../images/Dunitrust.256.png)

_29 novembre 2019_  
_RML #14_  
_Librelois <c@elo.tf>_

Suivre la présentation sur votre écran :

`librelois.duniter.io/slides/rml14/dunitrust`

---

layout: true
class: dark

.center[Dunitrust (Duniter-Rust)]

---

## Sommaire

1. [Historique du projet](#history)
2. [Codebase](#codebase)
3. [État fonctionnel](#functional-report)
4. [Depuis les rml13](#since-rml13)
5. [Quel SGBD ?](#sgbd)
6. [Roadmap pour les rml15..](#roadmap-rml15)
7. [.. et au-delà !](#roadmap)
8. [Comment rejoindre le projet ?](#how-to-join)
9. [Par quoi commencer ?](#where-to-start)
10. [Architecture du dépôt](#archi)
11. [Le système de modules](#module-system)
12. [Structure des données](#bc-db-1)
13. [Lecture BDD: une requête](#bc-db-rq)
14. [Écriture BDD: une requête](#bc-db-wq)
15. [Lecture BDD](#bc-db-r)
16. [Écriture BDD](#bc-db-w)
17. [API CLient GVA : démo](#gva-demo)
18. [API CLient GVA : live-coding](#gva-live-coding)

---

name: history

## .center[Historique du projet]

- Novembre 2017 Création du projet par Nanocryk.
  - ~ 3000 lignes. Crates crypto et wot.
- Février 2018: Rejoins par Éloïs.
- Mai 2018: Nanocryk quitte le projet pour prioriser `Fygg`.
- Fin mai 2018: [1ère prés. officielle de duniter-rs aux rml11](https://www.youtube.com/watch?v=fJvMRv1l5wM)
- Juillet 2018: v0.1
- Septembre 2018 : Renommage Duniter-Rs -> Durs (demande cgeek)
- Octobre 2018 : Nouveau contributeur: inso.
- Novembre 2018: [2ème prés. aux rml12](https://www.youtube.com/watch?v=EIjJNTeaP2g)
- Décembre 2018: Nouveau contributeur ji_emme.
- Mars 2019 : 1er hackathon dédié au projet.
- Avril 2019 : 3 nouveaux contributeurs : Hugo, Jonas et Tuxmain.
- Mai 2019: v0.2 + [3ème prés. aux rml13](https://www.youtube.com/watch?v=)
- Juin 2019: Nouveau contributeur: Lucas.
- Octobre 2019: 2ème hackathon dédié au projet.

---

name: codebase

## .center[Codebase]

- 323 fichiers
- 32 crates (31 lib + 1 bin)
- 220 tests (TU + TI)
- 34_820 lignes de code Rust (dont tests)
  - 1_059 TI
  - 3_183 TU
  - 11_342 instructions
  - 19_236 déclarations de types/trait (+ lignes non traversables)
- 9_207 lignes de commentaires
- ~900h de travail sur 18 mois

???

instructions obtenus via tarpaulin:

- sans TU : 4956/11342 (43,70 %)
- avec TU : 7988/14525 (54,99 %)

---

name: codebase2

## .center[Codebase : contributeurs]

- 34_825 lignes de code
  - Éloïs (84%)
  - Nanocryk (13%)
  - inso (1.5%)
  - Hugo (0.6%)
  - Jonas (0.3%)
  - ji_emme (0.1%)
  - Moul (0.05%)

---

name: functional-report

## .center[État fonctionnel]

- Synchronisation rapide depuis une blockchain JSON en local
- Un nœud miroir qui reste synchronisé sur le réseau
- une base de donnée indexant la blockchain
- WS2P Privé
- une interface graphique dans le terminal
- Un explorateur de BDD fournissant :
  - date d'expiration des membres
  - règle de distance des membres
  - solde d'un compte
  - liste des certifications d'un membre
- Une API client fournissant les méta-données des blocs

---

name: since-rml13

## .center[Depuis les rml13 (1/2)]

--

- Refactor du core pour préparer la sync par le réseau
--

- Externalisation des paramètres monétaires dans la crate currency-params
--

- Versionnage des blocs et des documents wot.
--

- Crypto: Complétion des tests pour couvrir toute la crate.
--

- Blockchain: requetage des blocs précédents pour remplir les trous dans l'arbre des forks.
--

- Restructuration de l'organisation des crates (et découpage crate documents)
--

- Ajout de la crate dubp-indexes (décrit les index dans le format du protocole)
--

- Dbex: ajout de la sous-commande `blocks` (ji_emme)
--

- Crypto: refonte pour utiliser ring (+ clear secrets at drop)
--

- Développement de la couche de sécurité PKSTL.
--

- Ajout du build d'un paquet arch (tuxmain)
--

- Ajout d'un audit automatique dans la CI (détection des failles de sécurités).
--

- Système de persistance des données: migration rustbreak -> lmdb
--

=> ~ 250h de travail sur 4 mois.

---

name: since-rml13

## .center[Depuis les rml13 (2/2)]

--

- migrate logger to fern to filter logs by crate #149
--

- configure logging system to display path of file #177
--

- Création de squelette de GVA avec query current (ji_emme)
--

- gva: configurer host & port
--

- gva: add queries node summary, block by number and blocks with paging
--

- gva: deep refactor to make the code testable
--

- gva: blocks: add inputs interval and step
--

- blockchain: création de TIs : `apply cautious` des 750 1ers blocs de la GT + revert
--

- Implémentation de la validation locale (Lucas et elois)
--

- Validation globale : implémentation du moteur de règles avec BR_G03 et BR_G100
--


=> ~ 80h de travail sur 1 mois et demi.

---

name: sgbd

## .center[Quel SGBD ?]

- Automne 2017 : Nanocryk resté en "mémory only"
--

- Printemps 2018 : utilisation de SQLite par mimétisme de Duniter
--

- Été 2018 : abandon de SQLite (trop lent), choix de rustbreak a défaut (le temps de trouver mieux)
--

- Automne 2018 : rien de mature ni à venir en pure Rust (sled alpha non financé et trop ambitieux) -> extension des recherches au monde C/C++
--

- Printemps 2019 : après 6 mois de réflexion, choix de LMDB (migration été 2019)
--

- Automne 2019 : [sled](https://github.com/spacejam/sled) en béta maintenant financé et avec des bench (crédibilisation des objectifs)

---

name: lmdb

## .center[LMDB: 5 raisons]

### .center[Lightning Memory-Mapped Database]

1. API Rust mature et maintenue par Mozilla
2. 100% ACID et transactionnel
3. Particulièrement optimisé pour la lecture
4. Accessible via plusieurs processus en //
5. Déjà plus de 10 ans, a fait ses preuves 
  - utilisé en backend de LDAP à choix égal avec Oracle
  - utilisé par Monero (une crypto qui focus sur privacy **et** scalabilité).

---

name: roadmap-rml15

## .center[Roadmap pour les rml15..]

- Jalon 0.3 (~ Janvier 2020) :
  - Correction des bugs bloquant #170 #171 #172 #183

- Jalon 0.4 (~ Juin 2020) :
  - Tests d'intégration de la synchro rapide, export des index (elois).
  - WS2P v2 synchronisation de la blockchain par le réseau (Hugo).
  - WS2P v2 rebond des fiches de peer, HEADs, et documents. (Hugo).
  - Client indexer : indexer et exposer les données blockchain pour GVA (elois).
  - Module GVA : nouvelle API Client GraphQL (ji_emme et jsprenger).
  - Persistance et maj différée de la fenêtre courante (1000i100 et elois).
  - Et plein d'autres petites taches... voir milestone sur le gitlab

---

name: roadmap

## .center[.. et au-delà !]

- Jalon 0.5 (~ fin 2020)
  - Développement du module Mempool
  - Finalisation de la validation globale (all rules)
  - WS2P v2 : Rebond des documents + requêtes v2 + synchro des mempool
  - GVA: Requêtes d'accès a la mempool

- Jalon 1.0 (~ mi 2021)
  - Module BlockGenerator + PoW
  - Interface d'administration
  - Variante dunitrust-desktop
  - Générateur de block genesis & code de lancement d'une monnaie
  - GVA: Requêtes de contrôle (merkel tree)

---

name: how-to-join

## .center[Comment rejoindre le projet ?]

- Avoir de bonnes notions de git (ou se former pour).
- Être rigoureux pour suivre des procédures a la lettre.
- Apprécier l'approche TDD (Développement piloté par les tests).
- Être prêt a faire des talk mumble (et équipé pour).
- Être très patient et voir long terme (vous ne serez pas opérationnel en moins de 6 mois).
- Être curieux, vouloir tout comprendre.
- Savoir chercher par vous-même d'abord.

--

=> Formation et transmission de connaissances via talk mumble.

---

name: where-to-start

## .center[Par quoi commencer ?]

Accompagnement individuel vers des taches simples et cadrés permettant de se familiariser avec la code base, notamment :

- Implémenter une règle de la validation globale.
- Ajouter un champ à une entité GVA.
- Ajouter une requête d'extraction de données dans dbex.
- Traduire un bout de la documentation.

=> Formation et transmission de connaissances via talk mumble.

---

name: archi

## .center[architecture du dépôt]

- .github : messages pour le dépôt miroir
- .gitlab : scripts python de publication des releases
- bin/ : crates binaires (durs-server, durs-desktop)
- doc/ : documentation haut niveau (pour dev ET utilisateurs)
- images/ : images statiques
- lib/ : crates bibliothèques (tools, core, modules)
  - core/ : crates constituant le squelette du programme (conf, boot, système modulaire)
  - dubp/ : Bibliothèques déclarant les entités du protocole DUBP.
  - dunp/ : Bibliothèques déclarant les entités du protocole DUNP.
  - modules/ : crates des modules
  - modules-lib/ : Bibliothèques utilisées par plusieurs modules.
  - tools : Bibliothèques externalisables (code non lié a l'architecture de durs)
- releases/ : scripts bash de build des paquets

---

name: module-system

## .center[Le système de modules]

![durs-modules-step-0](../../images/durs-modules-step-4.png)

- Le router filtre et relaie au(x) module(s) concernés.
- Destinataires ciblés via types de rôles et évents.

---

name: bc-db-1

## .center[Structure des données]

Paradigme Clé-Valeur. Chaque collection est nommée store.

LMDB défini 4 types de collections/stores :

- mono-valuée (Single)
- mono-valuée avec clés entières (SingleIntKey)
- multi-valuée (Multi)
- multi-valuée avec clés entières (MultiIntKey)

On peut découper les collections/stores de Dunitrust en 4 groupes fonctionnels :

- current metadata
- blocks (blockchain and forks)
- indexes (for global validation)
- client indexes (for client APIs)

---

name: bc-db-2

## .center[Structure des données (2)]

Current meta datas and blocks collections :

```text
Name               | StoreType    | KeyType            | ValueType
-------------------|--------------|--------------------|-----------------
CURRENT_METADATA   | SingleIntKey | u32                | ?
MAIN_BLOCKS        | SingleIntKey | BlockNumber        | BlockDb
ORPHAN_BLOCKSTAMP  | Single       | PreviousBlockstamp | Vec<Blockstamp>
FORK_BLOCKS        | Single       | Blockstamp         | BlockDb
```

```text
         CURRENT_METADATA
----------------------|------------
Key                   | ValueType
DbVersion             | u64
CurrencyName          | String
CurrentBlockstamp     | Blockstamp
CurrentBlockchainTime | u64
ForkTree              | ForkTree
NextWotId             | WotId

```

---

name: bc-db-3

## .center[Structure des données (3)]

Indexes collections :

```text
Name                   | StoreType    | KeyType            | ValueType
-----------------------|--------------|--------------------|----------
WOT_ID_INDEX           | Single       | PubKey             | WotId
IDENTITIES             | SingleIntKey | WotId              | DbIdentit
MBS_BY_CREATED_BLOCK   | MultiIntKey  | BlockNumber        | WotId
CERTS_BY_CREATED_BLOCK | MultiIntKey  | BlockNumber        | u64
DIVIDENDS              | Multi        | PubKey             | BlockNumber
UTXOS                  | Single       | UniqueIdUTXOv10    | TxOutput
CONSUMED_UTXOS         | SingleIntKey | BlockNumber        | Vec<UTXO>
```

---

name: bc-db-rq

## .center[Lecture BDD: une requête]

Récupération d'un bloc par son numéro :

```rust
/// Get BlockDb in local blockchain
pub fn get_db_block_in_local_blockchain<DB: BcDbInReadTx>(
    db_r: &DB,
    block_number: BlockNumber,
) -> Result<Option<BlockDb>, DbError> {
    db_r.db() // impl DbReadable
        .get_int_store(MAIN_BLOCKS) // IntegerStore
        .get(db_r.r(), block_number.0)? // Option<DbValue>
        .map(from_db_value) // Option<Result<BlockDb>>
        .transpose() // Result<Option<BlockDb>>
}
```

---

name: bc-db-wq

## .center[Écriture BDD: une requête]

Insertion d'un bloc dans la blockchain locale :

```rust
/// Insert new head Block in databases
/// Update MAIN_BLOCK only
pub fn insert_new_head_block(
    db: &Db,
    w: &mut DbWriter,
    block_db: BlockDb,
) -> Result<(), DbError> {
    // Serialize datas
    let bytes = durs_dbs_tools::to_bytes(&block_db)?; // Vec<u8>

    // Insert block in MAIN_BLOCKS store
    db.get_int_store(MAIN_BLOCKS).put(
        w.as_mut(),
        *block_db.block.number(),
        &Db::db_value(&bytes)?,
    ).map_err(DbError::StoreError)
}
```

---

name: bc-db-r

## .center[Lecture BDD : transaction]

BDD 100% ACID (donc transactionnelle).

Plusieurs requêtes dans une même transaction de lecture :

```rust
use durs_bc_db_reader::blocks::get_block_in_local_blockchain;
use durs_bc_db_reader::current_meta_datas::get_current_blockstamp;

let current_block: Option<BlockDocument> = db.r(|db_r| {
    if let Some(current_blockstamp) =
        get_current_blockstamp(db_r)? // Option<Blockstamp>
    {
        get_block_in_local_blockchain(
            db_r,
            current_blockstamp.id
        )? // Option<BlockDocument>
    } else {
        None // Option<_>
    }
})?;
```

---

name: bc-db-w

## .center[Écriture BDD : transaction ]

Plusieurs requêtes dans une même transaction d'écriture :

```rust
use durs_bc_db_writer::blocks::insert_new_head_block;
use durs_bc_db_writer::cur..meta_datas::update_current_meta_datas;

let new_valid_block: BlockDocument;
db.write(|mut w| {
    update_current_meta_datas(db, &mut w, &new_valid_block)?;
    insert_new_head_block(db, &mut w, None, BlockDb {
        block: new_valid_block,
        expire_certs: None,
    })?;
    Ok(w) // <- commit
})?;
```

---

name: gva-demo

## .center[API CLient GVA : démo]

https://dev.g1.dunitrust.org/graphiql

---

name: gva-live-coding

## .center[API CLient GVA : live coding]

Ajout du champ `membersCount` (nombre de membres).

---

## .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le gitlab de duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]
