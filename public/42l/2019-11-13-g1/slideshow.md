class: dark, middle, center

## La cryptomonnaie libre Ğ1: une blockchain atypique

![g1 logo](../../images/logo-g1.flare.256.png)

_13 novembre 2019_  
_École 42_  
_Librelois <c@elo.tf>_

Suivre la présentation sur votre écran :

### s.42l.fr/g1

---

layout: true
class: dark

### .center[La cryptomonnaie libre Ğ1]

---

## Sommaire (1/2)

* [La Ğ1](#g1)
* [Le protocole de la Ğ1](#dup)
* Écosystème technique de la Ğ1
  * [Écosystème général](#global)
  * Les serveurs blockchain
      * [Duniter](#duniter)
      * [Dunitrust](#dunitrust)
  * [Les clients (=wallets)](#clients)
  * [Les nombreux autres logiciels](#tools)
  * [Forge GitLab](#forge)
* La blockchain
  * [Livre de compte commun, transparent et infalsifiable](#blockchain)
  * [État vs transformation](#state-vs-transformation)
  * [Mécanisme de consensus](#consensus)
      * [La preuve de travail](#pow)
  * [Blockchain résumé](#blockchain-summary)
* Le cas du bitcoin
  * [les aspects positifs](#bitcoin-benefits)
  * [les inconvénients](#bitcoin-downsides)

---

## Sommaire (2/2)

* Spécificités du protocole de la Ğ1
  * [La toile de confiance](#wot)
  * [La PoW personnalisée](#personal-pow)
* DUP en détails
  * [DUBP et DUNP](#dubp-and-dunp)
  * DUBP
      * [Les documents utilisateur](#user-document)
      * [Les sources de monnaie](#sources)
      * [Découpage fonctionnel](#main-parts)
      * [Réception d'un bloc](#block-receipt)
      * [Validation locale vs globale](#local-vs-global)
      * [les index (résumé)](#indexes)
      * [résolution des forks](#resolution-fork)
  * [DUNP](#dunp)
* Questions

---

name: g1

## .center[La Ğ1 (prononcer "June")]

||||||
|:-:|:-:|:-:|:-:|:-:|
|![trm](../../images/trm.jpg)|=>|![duniter](../../images/Duniter.128.png)|=>|![g1 logo](../../images/logo-g1.flare.128.png)|
|TRM v1.0 publiée en **2010**||uCoin/Duniter né en **2013**||Ğ1/June lancée le 8 mars **2017**|

1ère monnaie libre (au monde !) au sens de la [Théorie Relative de la Monnaie (TRM)](http://trm.creationmonetaire.info/).

* 1er projet d'impl. UDP en 2011 (abandonné en 2013).
* 2ème projet d'impl. uCoin né en 2013 puis renommé Duniter en 2016.

La Ğ1 est co-créée par tous ses membres à part égale chaque jour. Cette part de création est nommée **Dividende universel**.

---

name: dup

# .center[Le protocole de la Ğ1]

## .center[DUP (DUniter Protocol)]

Protocole **blockchain** from scratch.

Transactions (comme les autres cryptos) + Spécificités :

* Co-création du dividende universel
* Toile de confiance permettant de garantir l'unicité des comptes membres de manière décentralisée.
* PoW économe grâce à la difficulté personnalisée.

---

name: global

### .center[Écosystème général]

.center[![g1-ecosystem](../../images/g1-ecosystem.jpg)]

---

name: duniter

### .center[Serveurs blockchain : Duniter]

.center[![duniter logo](../../images/Duniter.png)]

* Node.js/TypeScript avec quelques modules en C++
* En prod v1.7 (peu de dev, principalement maintenance)
* Référent : [cgeek](https://forum.duniter.org/u/cgeek)
* Contributeurs actifs* : 2

\*_A réalisé au moins un commit mergé dans les 6 derniers mois (hors documentation)._

???

Contributeurs actifs : contributions sur la documentation non comptabilisées.

---

name: dunitrust

### .center[Serveurs blockchain : Dunitrust]

.center[![dunitrust logo](../../images/Dunitrust.256.png)]

* Rust uniquement
* En développement actif v0.2 (prévision v1 courant 2021)
* Référent : [elois](https://forum.duniter.org/u/elois)
* Contributeurs actifs* : 5

\*_A réalisé au moins un commit mergé dans les 6 derniers mois (hors documentation)._

???

Contributeurs actifs : contributions sur la documentation non comptabilisées.

---

name: clients

## .center[Les clients]

| Logo | Nom | Langage | Avancement | Référent | Contributeurs actifs* |
|:-:|:-:|:-:|:-:|:-:|:-:|
| ![g1-ecosystem](../../images/Cesium.64.png)       | Cesium | Angular Js | En prod (v1.3.11) | [kimamila](https://forum.duniter.org/u/kimamila) | 5 |
| ![g1-ecosystem](../../images/Silkaj.white.64.png) | Silkaj | Python    | En prod (v0.7.3) | [moul](https://forum.duniter.org/u/moul)           | 3 |
| ![g1-ecosystem](../../images/Sakia.64.png)        | Sakia  | Python    | En dev (v0.33)  | [vit](https://forum.duniter.org/u/vit)              | 1 |
| ![g1-ecosystem](../../images/Cesium.64.png)       | Cesium v2 | Angular v7 | En dev (v2.x) | [kimamila](https://forum.duniter.org/u/kimamila) | 1 |

\*_Ayant réalisé au moins un commit mergé dans les 6 derniers mois._

---

name: tools

## Les nombreux autres logiciels

Visualisation de données :

* **g1-monit** (NodeJs) : stats monnaie et WoT ([elois](https://forum.duniter.org/u/elois) & [cgeek](https://forum.duniter.org/u/cgeek))
* **wotwizard** (Pascal) : stats WoT + proba d'entrées ([gerard94](https://forum.duniter.org/u/gerard94))
* **wotmap** (Js) : Visualiseur de la WoT ([paidge](https://forum.duniter.org/u/paidge))
* **WorldWotMap** (python) : Projection WoT sur carte ([tuxmain](https://forum.duniter.org/u/tuxmain))

Tools :

* **Gsper** (Js): récupérateur des identifiants ([1000i100](https://forum.duniter.org/u/1000i100))
* **G1sms** (bash) : paiement par sms : ([qoop](https://forum.duniter.org/u/frederic_renault))
* **dup-tools** (Rust&Js): validateur de documents blockchain ([elois](https://forum.duniter.org/u/elois))
* **Gmix** (python) : mixeur de sources ([tuxmain](https://forum.duniter.org/u/tuxmain))
* **Barre de contribution intégrable** (Php&Js) : ([paidge](https://forum.duniter.org/u/paidge))

Et bien d'autres encore…

---

name: forge

### Retrouvez tous les projets sur notre forge GitLab

# .center[[git.duniter.org](https://git.duniter.org)]

___

Création de compte GitLab **uniquement sur demande** :

* Écrire un mail à [contact@duniter.org](mailto:contact@duniter.org)

ou

* Demander sur le forum technique : [forum.duniter.org](https://forum.duniter.org)

---

name: blockchain

### .center[La blockchain]

Un grand livre de compte commun, transparent et infalsifiable

|||
|:-:|:-:|
| ![bc-registre-ex1](../../images/bc-registre-ex1.png) | ![bc-livres](../../images/bc-livres.png) |
--
|||
| Résume -> Hash | ![blockchain](../../images/blockchain1.png) |

---

name: state-vs-transformation

# .center[État vs transformation]

La blockchain ne comporte que des transformations d'états.

<div class="mermaid">
graph LR
    bloc-genesis-->E1..En
    E1..En-->E'1..E'n
    etat0-->etat1
    etat1-->etat2
</div>

Bloc = transformations = liste d'événements.

État = Index -> persistés en base de donnée

Appliquer un bloc = Appliquer les transformations du bloc aux index décrivants l'état courant.

---

name: consensus

## .center[Mécanisme de consensus]

<div class="mermaid">
graph RL
        1#A-->0#A
        2#A-->1#A
        3#A-->2#A
        2#B-->1#A
        3#B-->2#B
        3#C-->2#A
</div>

* Si plusieurs blocs valides, lequel choisir ?
--

  * => Besoin de limiter qui peut écrire le prochain bloc
--

  * => Besoin de résoudre les forks

---

name: pow

## .center[La preuve de travail]

|||
|:-:|:--|
|![machines map monde](../../images/proof-of-work.jpg)|-> On incrémente un paramètre (nonce), et on réessaye jusqu’à obtenir un hash qui respecte un certain motif. -> La difficulté correspond à l'improbabilité de tomber sur le motif.|

--
=> Permet de choisir un temps moyen cible entre deux blocs.

---

name: blockchain-summary

## .center[Blockchain : résumé]

Une blockchain **est** un registre d’événements décentralisé et commun.

Nécessite :  

--
-> Un format pour chaque bloc de ce registre  

--
-> Des règles métier qui spécifient ce qu'est un bloc "valide"  

--
-> Un mécanisme de consensus  

--
-> Un algorithme de résolution des forks

--
-> Un protocole réseau (pour synchroniser les nœuds)

---

name: bitcoin-benefits

## .center[Le cas du bitcoin: les +]

* 1ère monnaie entièrement basée sur un réseau libre et P2P
  * -> donc décentralisée
  * -> donc liberté d'usage
--

* 1ère monnaie réellement sécurisée basée sur des concepts cryptographiques inviolables. Une prouesse technique.

---

name: bitcoin-downsides

## .center[Le cas du bitcoin: les -]

* Monnaie créée uniquement par des machines (humain absent du protocole)
--

* Création asymétrique car basée sur la puissance de calcul → donc la capacité à investir dans des fermes de calcul en monnaie dette…
--

* Catastrophe écologique (les fermes bitcoin consomment plus de 4 centrales nucléaires !)
--

* Catastrophe économique : courbe de création logarithmique -> induit raréfaction et spéculation

---

name: wot

## .center[La toile de confiance (1/3)]

Nécessaire pour s'assurer que 1 Humain = 1 DU

Système d'identification décentralisé -> ce sont les membres qui se certifient entre eux.

Repose sur la densité des graphes sociaux ([théorie des six poignées de main](https://fr.wikipedia.org/wiki/Six_degr%C3%A9s_de_s%C3%A9paration)).

.center[![sybil1](../../images/sybil1-r1.jpg)]

---

## .center[La toile de confiance (2/3)]

Résumé des règles :

* Obtenir 5 certifications en moins de 2 mois.
* Être à moins de 5 pas de 80% des membres référents.
* Renouveler son adhésion 1 fois par an (1 clic).
* Avoir au moins 5 certifications actives à tout instant t (=à chaque bloc)


Tous les détails dans l'article dédié :  
[La toile de confiance en détail](https://duniter.org/fr/la-toile-de-confiance-en-d%C3%A9tail/) / [Deep-dive into the Web of Trust](https://duniter.org/en/deep-dive-into-the-web-of-trust/).

Conférence sur la toile de confiance Ğ1 : [peertube](https://tube.p2p.legal/videos/watch/2e593883-e5ef-4e14-9f3e-912d9fb76916) / [youtube](https://www.youtube.com/watch?v=8GaTKfa-ADU).

---

Application de visualisation : https://wotmap.duniter.org

.center[![wot-2019-11-11-elois](../../images/wot-2019-11-11-elois.png)]

---

name: personal-pow

## .center[La PoW personnalisée (1/3)]

* L'écriture des blocs est réservée aux membres de la toile de confiance

=> Permet d'assigner une difficulté personnalisée à chaque membre forgeron

--

* Fenêtre courante (nombre de blocs pris en compte)
  * définition différentielle
--

* Facteur d'exclusion
  * `exFact = MAX[1; FLOOR (0.67 * nbPreviousIssuers / (1 + nbBlocksSince))]`
--

* Handicap
  * `handicap = FLOOR(LN(MAX(1;(nbPersonalBlocks + 1) / median)) / LN(1.189))`

--

=> `diff = powMin*exFact + handicap`


Article détaillé sur la PoW de la Ğ1 : [Preuve de travail](https://duniter.org/fr/wiki/duniter/preuve-de-travail/) / [The Proof-of-work](https://duniter.org/en/wiki/duniter/2018-11-27-duniter-proof-of-work/)

---

## .center[La PoW personnalisée (2/3)]

.center[![g1-fc-270959](../../images/g1-fc-270959.png)]

---

```
Current block: n°270959, generated on the 2019-11-11 23:17:00
Common Proof-of-Work difficulty level: 89, hash starting with `00000[0-6]*`
|       uid        |        match         |   Π diffi    |   Σ diffi |
|------------------+----------------------+--------------+-----------|
|       moul       | 00000000000000000000 | 7.6 × 10^140 |      1872 |
|      LenaB       | 00000000000000000000 | 1.9 × 10^67  |       891 |
|     gerard94     | 00000000000000000000 | 9.1 × 10^46  |       624 |
|     pafzedog     | 00000000000000000000 | 5.2 × 10^33  |       448 |
|    Mententon     | 00000000000000000000 | 1.5 × 10^27  |       357 |
|     ji_emme      | 0000000000000000[0-1 | 2.6 × 10^20  |       270 |
|      Rykian      | 0000000000000000[0-1 | 2.6 × 10^20  |       270 |
|      Petrus      |  00000000000[0-A]*   | 8.8 × 10^13  |       181 |
|   mollenthiel    |  00000000000[0-A]*   | 8.8 × 10^13  |       181 |
|   WilliamWeber   |  00000000000[0-D]*   | 3.5 × 10^13  |       178 |
|  MarcelDoppagne  |     00000[0-2]*      |  1.4 × 10^7  |        93 |
|     charles      |     00000[0-2]*      |  1.4 × 10^7  |        93 |
|      Colisa      |     00000[0-2]*      |  1.4 × 10^7  |        93 |
|  BenoitLavenier  |     00000[0-4]*      |  1.2 × 10^7  |        91 |
|     Paulart      |     00000[0-5]*      |  1.0 × 10^7  |        90 |
...
|       poka       |     00000[0-6]*      |  9.4 × 10^6  |        89 |
|      elois       |     00000[0-6]*      |  9.4 × 10^6  |        89 |
|     1000i100     |     00000[0-6]*      |  9.4 × 10^6  |        89 |
|    b_presles     |     00000[0-6]*      |  9.4 × 10^6  |        89 |
|     oaktree      |     00000[0-6]*      |  9.4 × 10^6  |        89 |
```

---

name: dubp-and-dunp

# .center[DUBP et DUNP]

Historiquement DUP désignait uniquement le protocole **blockchain**.

-> Or une crypto-monnaie **ne peut pas** fonctionner sans protocole réseau.

DUBP : Blockchain Protocol (v11) : [spécifications](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md)

DUNP : Network Protocol (v1) : [spécifications](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0004_ws2p_v1.md)

---

name: user-document

# .center[Les Documents Utilisateur]

Chaîne de car. signée par la clé privée de l'utilisateur.

5 types de documents utilisateur :

* Identity
* Membership
* Certification
* Revocation
* Transaction

Exemple de chaque document sur [dup-tools-front](https://dup-tools-front.duniter.io/)

--

Différents formats :

* Format raw ou plain text : pour signer/vérifier
* Format compact text : dans un bloc

---

name: sources

# .center[Les sources de monnaie]

## .center[création]

2 types de sources :

1. Les Dividendes Universels :

    Créés dans un bloc contenant un champ '`dividend`' valorisé.
    Un tel bloc est généré toutes les '`dt`' secondes (86400 pour la Ğ1).

2. Les UTX0 (Unused Transaction Output) :

    ```yml
    AMOUNT:BASE:CONDITIONS
    30000:0:SIG(PUBKEY)
    10500:0:(SIG(PUBKEY1) && XHX(HASH)) || SIG(PUBKEY2)
    500000:0:SIG(PUBKEY1) || (SIG(PUBKEY2) && CSV(DELAY))
    800000:0:SIG(PUBKEY1) || (SIG(PUBKEY2) && CLTV(TIMESTAMP))
    ```

---


name: sources2

# .center[Les sources de monnaie]

## .center[preuve d'utilisabilité]

Preuves publiées dans la section 'Unlocks' de la transaction

* Format des preuves d'une source :

    ```yml
    INPUT_INDEX:PROOF1 PROOF2 .. PROOFn
    ```

* Format d'une preuve :

    ```yml
    SIG(ISSUER_INDEX)
    XHX(CODE)
    ```

---

name: sources3

# .center[Les sources de monnaie]

## .center[destruction]

Destruction actée par la déclaration dans un input d'une transaction :

1. Les Dividendes Universels :

    ```yml
    AMOUT:BASE:D:PUBLIC_KEY:BLOCK_NUMBER
    1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937
    ```

2. Les UTX0 (Unused Transaction Output) :

    ```yml
    AMOUNT:BASE:T:TX_HASH:OUTPUT_INDEX
    80000:0:T:A5415E194A6A39D9CEC6D06C797D6AB3169BF9C14E65133F42F0E4347847B3EE:0
    ```

---

name: main-parts

# .center[schéma général]

<div class="mermaid">
graph LR
    W(Inter-nodes API) --- B(Blockchain)
    W --- P(Mempool)
    P-->G(Block generator)
    B --- G
    C(Client API) --- P
    C --- B
    C --- W
    C --- I(Client indexer)
    B --> I
</div>

Blockchain : Vérifie la validité des blocs et les "applique".  

--
Block generator : génère contenu prochain bloc + PoW  

--
Client API : reçoit doc + req (histo tx, sources/compte, etc)

--
Client indexer : indexe sources et historique tx par compte.

--
inter-node API : synchro avec les autres nœuds.  
Mempool : Stocke documents utilisateurs temporairement.

---

name: block-receipt

### .center[Réception d'un bloc]

<div class="mermaid">
sequenceDiagram
    local node->>local node: Local validation
    local node->>local node: Global validation (check all rules)
    alt valid
        local node->>local node: Apply block
        local node->>others nodes: relay valid block
    else invalid
        local node->>local node: tag blockstamp as invalid
    end
</div>

---

name: local-vs-global

## .center[Validation Locale vs Globale]

### -> Validation locale

* Vérifie cohérence d'un bloc bien formaté, sans autre contexte que le bloc lui-même.

### -> Validation Globale

* Vérifie cohérence d'un bloc validé localement, dans le contexte de l'ensemble de la blockchain.
* S'effectue via l'application de **108 règles** spécifiées [*ici*](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0009_Duniter_Blockchain_Protocol_V11.md#br_g01-headnumber).
* C'est dans cette phase que les "index" entrent en jeux.

---

name: indexes

# .center[les index]

* IINDEX : index des identités

    ```csv
    op, uid, pub, created_on, written_on, member, was_member, kick
    ```

* MINDEX : index des adhésions

* CINDEX : index des certifications

* SINDEX : index des sources de monnaie

Conférence détaillée sur chaque index : [youtube](https://www.youtube.com/watch?v=VQuksme9s88&list=PLMREUqep567viO6NigNeQNIu4dlCc1JxY&index=4).

---

name: resolution-fork

## .center[Résolution des Forks]

R1 : La branche la plus longue l'emporte.  
R2 : Pour changer de branche, la nouvelle doit avoir au moins 3 blocs d'avance **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs inclus.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3-->A2
        B2-->A1
        C3-->A2
        C4-->C3
        C5-->C4
</div>

--
### .center[=> Le node A reste sur sa branche]

---

## .center[Résolution des Forks (2)]

R1 : La branche la plus longue l'emporte.  
R2 : Nouvelle branche 3 blocs **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs inclus.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3-->A2
        B2-->A1
        B3-->B2
        B4-->B3
        B5-->B4
        B6-->B5
        C3-->A2
        C4-->C3
        C5-->C4
</div>

--
#### .center[=> Le node A rollback 2 blocs puis empile la chaîne B]

---

## .center[Résolution des Forks (3)]

R1 : La branche la plus longue l'emporte.  
R2 : Pour changer de branche, la nouvelle doit avoir au moins 3 blocs d'avance **et** 15 minutes d'avance.  
R3 : Rollback maximum de 100 blocs inclus.

<div class="mermaid">
graph RL
        A1-->A0
        A2-->A1
        A3..A101-->A2
        A102-->A3..A101
        B2-->A1
        B3..B103-->B2
        B104-->B3..B103
        B105-->B104
        B106-->B105
        C3..C103-->A2
        C104-->C3..C103
        C105-->C104
</div>

--
#### .center[=> Le node A rollback 100 blocs puis empile la chaîne C]

---

name: dunp

# .center[Le protocole réseau]

* Basé sur WS2P (WebSocket To Peer) v1 : [spécifications](https://git.duniter.org/nodes/common/doc/blob/master/rfc/0004_ws2p_v1.md)
* connexions permanentes pour synchroniser :
  * les blocs
  * les documents utilisateurs
  * les mempools des nodes
  * les fiches de pairs (peers)
* 2 types de nodes : WS2P Public & WS2P Privé
  * WS2P public = avec endpoint
  * WS2P Privé = sans endpoint
* Les nodes avec WS2P Public ont aussi WS2P Privé d'activé.
* Chaque node a un quota de connexions limitées (faible en entrée et élevé en sortie).

-> [Présentation de WS2Pv2](https://librelois.duniter.io/ws2pv2-rml12/)

---

# .center[Merci de votre attention]

Présentation réalisée avec [remark](https://github.com/gnab/remark).  
Graphes réalisés avec [mermaid](https://github.com/knsv/mermaid).

Retrouvez les sources de cette présentation sur le GitLab de Duniter :

.center[[https://git.duniter.org/librelois/slides](https://git.duniter.org/librelois/slides)]
